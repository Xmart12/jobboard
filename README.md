# Job Board CRUD Test

## Getting Started

This project contains a solution for JobBoard divided in Backend (WebAPI) and Frontend (WebClientApp)

### Projects Included

* **Backend:** JobBoardAPI - *(ASP.NET Core 2.1 WebAPI)*
* **Frontend:** jobboard-app - *(Node.js React App)*

### Technologies used

* Web API: ASP.NET Core v2.1
* Database: SQLite
* Packages: EntityFrameworkCore, EntityFrameworkCore.SQLite (for data modeling)
* Node.js with React.js

## Run Instructions 

Clone repository with ```git clone https://gitlab.com/Xmart12/jobboard.git```

### Backend Application Settings

* **Using Visual Studio IDE:** Open JobBoardAPI solution in Backend folder and restore Packages from *Pakages Manager Console* with the command: ```Update-Package```. Build and Run Application 

* **Using Command Promt:** Run the following commands in BackendFolder in order to run the application:

```
dotnet restore
dotnet build
dotnet run
```

* Set connectionString (if necessary) in ```appsettings.json```, for example:

``` 
"ConnectionStrings": {
    "DefaultConnection": "DataSource=jobsdb.db;"
  },  
```

Run WebAPI Application

### Frontend Application Settings

* Run ```npm install``` command in Frontend folder in order install node modules used in WebApp

* Run ```npm start``` for run the Application

* Set URL Web API (if necessary) in ```/src/config.js```, for example:

```
const config = {
    urlAPI: 'https://localhost:44386/api'
}
```