import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { FaBriefcase, FaSuitcase } from 'react-icons/fa';
import { LinkContainer } from 'react-router-bootstrap';


export default props => (

    <Navbar bg="dark" variant="dark" sticky="top">
        <LinkContainer to={'/'}>
            <Navbar.Brand><FaBriefcase/> {' '} Job Board</Navbar.Brand>
        </LinkContainer>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <LinkContainer to={'/jobs'}>
                    <Nav.Link><FaSuitcase /> Jobs</Nav.Link>
                </LinkContainer>
            </Nav>
        </Navbar.Collapse>
    </Navbar>

);