import http from '../http';

class JobsDataService {

    getAll(params) {

        var url = '/jobs';

        if (params.length > 0)
        {
            url += '?';
            params.forEach((par) => {
                url += par.name + '=' + par.value + '&';
            });
        }

        return http.get(url);
    }

    getById(id) {
        return http.get(`/jobs/${id}`);
    }

    create(data) {
        return http.post('/jobs', data);
    }

    update(id, data) {
        return http.put(`/jobs/${id}`, data);
    }

    delete(id) {
        return http.delete(`/jobs/${id}`);
    }

    getResponseErrors(data) {

        var errMessage = '';

        for (var key in data) {
            errMessage += '* ' + data[key] + ' *';
        }

        return errMessage;
    }


}

export default new JobsDataService();