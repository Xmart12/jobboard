import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import NavMenu from './components/navmenu';
import Home from './modules/home';
import Jobs from './modules/jobs';

export default props => (
    
    <Router>
        <NavMenu />
        <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/jobs' component={Jobs} />
        </Switch>
    </Router>

);