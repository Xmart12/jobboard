import React from 'react';
import Moment from 'moment';
import { Container, Col, Row, Form, Table, Button, Card, Alert, Pagination } from 'react-bootstrap';
import { FaSuitcase, FaPlus, FaEdit, FaTrashAlt, FaSave, FaTimesCircle } from 'react-icons/fa';

import JobDataService from '../services/jobservices';

class Jobs extends React.Component {

    //Component state
    state = {
        jobs: [],
        jobFormVisible: false,
        formUpdate: false,
        formErrorsVisible: false,
        formErrorMessage: '',
        generalErrorVisible: false,
        generalErrorMessage: '',
        paginationData: {}
    };

    //Current selected page
    currentPage = 1;

    //Clean and Hide JobForm
    cleanForm() {

        if (this.state.jobFormVisible) {
            this.refs.id.value = "";
            this.refs.code.value = "";
            this.refs.title.value = "";
            this.refs.description.value = "";
            this.refs.createdAt.value = "";
            this.refs.expiresAt.value = "";
        }

        this.setState({ 
            formErrorsVisible: false,
            formErrorMessage: '',
            formUpdate: false,
            generalErrorVisible: false,
            generalErrorMessage: ''
         });
    }

    //Set Form for Update Data
    updateForm(id) {

        this.cleanForm();

        JobDataService.getById(id).then(res => {
            this.setState({ jobFormVisible: true, formUpdate: true });
                
            this.refs.id.value = res.data.id;
            this.refs.code.value = res.data.code;
            this.refs.title.value = res.data.title;
            this.refs.description.value = res.data.description;
            this.refs.createdAt.value = res.data.createdAt;
            this.refs.expiresAt.value = Moment(res.data.expiresAt).format('YYYY-MM-DD');
        }).catch(e => {
            if (e.response) {
                this.setState({
                    generalErrorVisible: true,
                    generalErrorMessage: 'Error ' + JobDataService.getResponseErrors(e.response.data)
                });
            }
            else {
                console.log(e);
            }
        })
    }

    //Handle Create Update Job Form 
    handleSubmit(event) {
        event.preventDefault();

        var id = this.refs.id.value;

        //Get Form Data
        var job = {
            code: this.refs.code.value,
            title: this.refs.title.value,
            description: this.refs.description.value,
            createdAt: this.refs.createdAt.value,
            expiresAt: this.refs.expiresAt.value
        };

        //Get Data ID for Update
        if (id) {   
            //Update Action
            job.id = id;
            job.expiresAt = (job.expiresAt !== "") ? job.expiresAt : new Date();
            this.updateJob(id, job);
        }
        else {
            //Create Action
            job.createdAt = new Date();
            job.expiresAt = (job.expiresAt !== "") ? job.expiresAt : Moment(new Date()).format('YYYY-MM-DD');
            this.createJob(job);
        }
    }

    //Get Jobs Data
    componentDidMount() {
        this.getQueryParam();
    }

    //Get Query Filter values
    getQueryParam() {

        var params = [
            { name: "q", value: this.refs.query.value },
            { name: "sort", value: this.refs.sort.value },
            { name: "page", value: this.currentPage },
            { name: "pagesize", value: this.refs.pagesize.value }
        ]

        this.getJobs(params);
    }

    //Get Job List
    getJobs(params) {
        JobDataService.getAll(params).then(res => { 
            this.setState({ 
                jobs: res.data, 
                paginationData: JSON.parse(res.headers['x-pagination']) });
        }).catch(e => {
            if (e.response) {
                this.setState({
                    generalErrorVisible: true,
                    generalErrorMessage: 'Error ' + JobDataService.getResponseErrors(e.response.data)
                });
            } 
            else {
                console.log(e);
            }
        });
    }  

    //Create Job
    createJob(job) {
        JobDataService.create(job).then(res => {
            this.cleanForm();
            this.setState({ jobFormVisible: false });
            this.getQueryParam();
        }).catch(e => {
            if (e.response) {
                this.setState({
                    formErrorsVisible: true,
                    formErrorMessage: 'Error: ' + JobDataService.getResponseErrors(e.response.data)
                });
            }
            else {
                console.log(e);
            }
        });
    }

    //Update Job
    updateJob(id, job) {
        JobDataService.update(id, job).then(res => {
            this.cleanForm();
            this.setState({ jobFormVisible: false });
            this.getQueryParam();
        }).catch(e => {
            if (e.response) {
                this.setState({
                    formErrorsVisible: true,
                    formErrorMessage: 'Error: ' + JobDataService.getResponseErrors(e.response.data)
                });
            } 
            else {
                console.log(e);
            }
        });
    }

    //Delete Job
    deleteJob(job) {

        this.cleanForm();
        this.setState({ jobFormVisible: false });

        if (window.confirm('Are you sur to delete this job?')) {
            JobDataService.delete(job.id).then(res => {
                this.getQueryParam();
            }).catch(e => {
                if (e.response) {
                    this.setState({
                        generalErrorVisible: true,
                        generalErrorMessage: 'Error ' + JobDataService.getResponseErrors(e.response.data)
                    });
                }
                else {
                    console.log(e);
                }
            });
        }

    }

    //Return Pagination
    pagintation(pagindata) {

        let pages = [];

        for (let i = 1; i <= pagindata.TotalPages; i++) {
            pages.push(<Pagination.Item key={i} onClick={ () => { this.setPage(i); } } active={i === pagindata.PageIndex}>{i}</Pagination.Item>);
        }

        return pages;
    }

    //Get selected page
    setPage(page) {
        this.currentPage = page;
        this.getQueryParam();
    }


    //Render Component
    render() {

        const { jobs, jobFormVisible, formUpdate, formErrorsVisible, formErrorMessage, generalErrorVisible, generalErrorMessage, paginationData } = this.state;

        return (

            <Container fluid>
                <br/>
                <Row>
                    <Col><h3><FaSuitcase/> Jobs</h3></Col>
                </Row>
                <Row>
                    <Col md={4}>
                        <label>Search job:</label>
                        <Form.Control ref="query" placeholder="Search for job" onChange={ () => this.getQueryParam() } />
                    </Col>
                    <Col md={5}></Col>
                    <Col md={2}>
                        <label>Sort by:</label>
                        <select ref="sort" className="form-control" onChange={ () => this.getQueryParam() }>
                            <option value="">Code</option>
                            <option value="title">Title</option>
                            <option value="title_desc">Title Desc</option>
                            <option value="description">Description</option>
                            <option value="expires">Expires At</option>
                        </select>
                    </Col>
                    <Col md={1}>
                        <label>Page Size:</label>
                        <select ref="pagesize" className="form-control" onChange={ () => this.getQueryParam() }>
                            <option value="5">5</option>
                            <option value="10">10</option>
                        </select>
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col md={3}>
                        <Button onClick={ () => { this.setState({ jobFormVisible: true }); this.cleanForm(); }}><FaPlus/> New Job</Button>
                    </Col>
                    <Col md={9}>
                        { generalErrorVisible && <Alert variant="danger">{ generalErrorMessage }</Alert> }
                    </Col>
                </Row>
                { jobFormVisible && <Row style={{ marginTop: 10 }}>
                    <Col>
                        <Form onSubmit={ (event) => { this.handleSubmit(event); }}>
                            <Card>
                                <Row style={{ margin: 5 }}>
                                    <Form.Control ref="id" type="hidden" />
                                    <Form.Control ref="createdAt" type="hidden" />
                                    <Col md={2}>
                                        <Form.Label>Job Code:</Form.Label>
                                        <Form.Control ref="code" disabled={ formUpdate } />
                                    </Col>
                                    <Col md={3}>
                                        <Form.Label>Job Title:</Form.Label>
                                        <Form.Control ref="title" />
                                    </Col>
                                    <Col md={5}>
                                        <Form.Label>Description:</Form.Label>
                                        <Form.Control ref="description" />
                                    </Col>
                                    <Col md={2}>
                                        <Form.Label>Expires At:</Form.Label>
                                        <Form.Control ref="expiresAt" type="date" />
                                    </Col>
                                </Row>
                                <Row style={{ margin: 10 }}>
                                    <Col md={2}></Col>
                                    <Col md={8}>
                                        { formErrorsVisible && <Alert variant="danger">{ formErrorMessage }</Alert> }
                                    </Col>
                                    <Col md={2}>
                                        <Button variant="outline-info" type="submit"><FaSave/> Save</Button>
                                        { ' ' }
                                        <Button variant="outline-dark" onClick={ () => {  this.cleanForm(); this.setState({ jobFormVisible: false }); }}><FaTimesCircle/> Cancel</Button>
                                    </Col>
                                </Row>
                            </Card>
                        </Form>
                    </Col>
                </Row> }
                <br/>
                <Row>
                    <Table>
                        <thead>
                            <tr>
                                <th>Job Code</th>
                                <th>Job Title</th>
                                <th>Description</th>
                                <th>Created At</th>
                                <th>Expires At</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                jobs.map(job => (
                                    <tr key={ job.id }>
                                        <td>{ job.code }</td>
                                        <td>{ job.title }</td>
                                        <td>{ job.description }</td>
                                        <td>{ Moment(job.createdAt).format('DD/MM/YYYY') }</td>
                                        <td>{ Moment(job.expiresAt).format('DD/MM/YYYY') }</td>
                                        <td>
                                            <Button variant="info" size="sm" onClick={ () => this.updateForm(job.id) }><FaEdit/> Edit</Button>
                                            {' '}
                                            <Button variant="danger" size="sm" onClick={ () => this.deleteJob(job) }><FaTrashAlt/> Delete</Button>
                                        </td> 
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </Row>
                <Row>
                    <Col>
                        <Pagination>{ this.pagintation(paginationData) }</Pagination>
                    </Col>
                </Row>
            </Container>

        );

    }

}

export default Jobs;