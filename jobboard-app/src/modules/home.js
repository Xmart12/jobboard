import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { FaHome } from 'react-icons/fa';

class Home extends React.Component {

    render() {

        return (

            <Container fluid>
                <br/>
                <Row><Col><h1><FaHome/> Home</h1></Col></Row>
                <br/>
                <Row>
                    <Col>
                        <p>Job Board Home</p>
                        <p>Press <b>Jobs</b> Menu for Job list.</p>
                    </Col>
                </Row>
            </Container>

        );

    }
}

export default Home;