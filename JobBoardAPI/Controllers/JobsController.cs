﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobBoardAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace JobBoardAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/jobs")]
    [ApiController]
    public class JobsController : Controller
    {
        //DBContext Instance
        private readonly ApplicationDbContext db;


        //Jobs Constructor
        public JobsController(ApplicationDbContext context)
        {
            this.db = context;
        }


        // List of Jobs
        // GET /api/jobs
        [HttpGet]
        public IActionResult GetListJobs(string q, string sort, int? page, int? pagesize)
        {
            var jobs = db.Jobs.ToList();

            //Query Search String
            if (!String.IsNullOrWhiteSpace(q))
            {
                q = q.ToLower();

                jobs = jobs.Where(w => w.Code.ToLower().Contains(q) || w.Title.ToLower().Contains(q) || w.Description.ToLower().Contains(q))
                    .ToList();
            }

            //Sort String
            switch (sort)
            {
                case "title":
                    jobs = jobs.OrderBy(o => o.Title).ToList();
                    break;
                case "title_desc":
                    jobs = jobs.OrderByDescending(o => o.Title).ToList();
                    break;
                case "description":
                    jobs = jobs.OrderBy(o => o.Description).ToList();
                    break;
                case "expires":
                    jobs = jobs.OrderBy(o => o.ExpiresAt).ToList();
                    break;
                default:
                    jobs = jobs.OrderBy(o => o.Code).ToList();
                    break;
            }

            //Paging data
            PaginatedList<Job> data = new PaginatedList<Job>(jobs, (page ?? 1), (pagesize ?? 5));

            //Set Paging Metadata
            var pagingdata = new { data.TotalCount, data.PageSize, data.TotalPages, data.PageIndex, data.HasNextPage, data.HasPreviousPage };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(pagingdata));

            //Create Paging Data Content
            data = data.Create();

            //Paging and Return 
            return Ok(data);

        }


        // Especified job by id
        // GET /api/jobs/{id}
        [HttpGet("{id}", Name = "GetJob")]
        public IActionResult GetJobById(int id)
        {
            //Get Job
            var job = db.Jobs.FirstOrDefault(f => f.Id == id);

            //Job not found
            if (job == null)
            {
                return NotFound();
            }

            return Ok(job);
        }


        // Create new job
        // POST /api/jobs
        [HttpPost]
        public IActionResult CreateJob([FromBody] Job job)
        {
            //Model state is not valid
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Verify Expires Date
            if (job.ExpiresAt < DateTime.Today)
            {
                ModelState.AddModelError("Date Error", "Expires At can't be lower than Today");
                return BadRequest(ModelState);
            }

            //Set Clean Code Job
            job.Code = job.Code.Trim().ToUpper();

            //Verifiy Unique Job Code
            var jobcode = db.Jobs.FirstOrDefault(f => f.Code == job.Code);
            if (jobcode != null)
            {
                ModelState.AddModelError("Existing Register", "Job Code already exists");
                return BadRequest(ModelState);
            }

            //Save in DB
            db.Jobs.Add(job);
            db.SaveChanges();

            return new CreatedAtRouteResult("GetJob", new { id = job.Id }, job);

            
        }
    

        // Update job
        // PUT /api/jobs/{id}
        [HttpPut("{id}")]
        public IActionResult UpdateJob([FromBody] Job job, int id)
        {
            //Verify Ids
            if (job.Id != id)
            {
                return BadRequest();
            }

            //Model state is not valid
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Verify Expires Date
            if (job.ExpiresAt < DateTime.Today)
            {
                ModelState.AddModelError("Date Error", "Expires At can't be lower than Today");
                return BadRequest(ModelState);
            }

            //Update in DB
            db.Entry(job).State = EntityState.Modified;
            db.SaveChanges();

            return Ok();
        }


        // Delete job
        // DELETE /api/jobs/{id}
        [HttpDelete("{id}")]
        public IActionResult DeleteJob(int id)
        {
            //Get DB Item
            var job = db.Jobs.FirstOrDefault(f => f.Id == id);

            //Job not found
            if (job == null)
            {
                return NotFound();
            }

            //Delete from DB
            db.Jobs.Remove(job);
            db.SaveChanges();

            return Ok(job);
        }

    }
}