﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace JobBoardAPI.Models
{
    public class Job
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MaxLength(10)]
        public string Code { get; set; }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [ReadOnly(true)]
        public DateTime CreatedAt { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ExpiresAt { get; set; }
    }
}
