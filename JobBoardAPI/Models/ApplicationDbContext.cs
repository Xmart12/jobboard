﻿using Microsoft.EntityFrameworkCore;

namespace JobBoardAPI.Models
{
    public class ApplicationDbContext : DbContext
    {

        //Application DB Context Construct
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }


        //Jobs DataSet
        public DbSet<Job> Jobs { get; set; }


    }
}
