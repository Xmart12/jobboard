﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JobBoardAPI.Models
{
    public class PaginatedList<T> : List<T>
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }


        public PaginatedList(List<T> items, int pageIndex, int pageSize)
        {
            int count = items.Count();

            TotalCount = count;
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            PageSize = pageSize;

            this.AddRange(items);
        }


        public bool HasPreviousPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }


        public bool HasNextPage
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }


        public PaginatedList<T> Create()
        {
            var count = this.Count();
            var items = this.Skip((this.PageIndex - 1) * this.PageSize).Take(this.PageSize).ToList();
            return new PaginatedList<T>(items, this.PageIndex, this.PageSize);
        }

    }
}
